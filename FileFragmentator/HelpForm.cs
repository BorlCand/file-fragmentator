﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FileFragmentator {
	public partial class HelpForm : Form {
		public HelpForm() {
			InitializeComponent();
		}

		private void HelpForm_FormClosed(object sender, FormClosedEventArgs e) {
			this.Dispose();
		}
	}
}
