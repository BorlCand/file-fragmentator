﻿namespace FileFragmentator {
	partial class Main {
		/// <summary>
		/// Требуется переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Обязательный метод для поддержки конструктора - не изменяйте
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.splitTabSplitFileBrowse = new System.Windows.Forms.Button();
			this.splitTabTextBoxSplitFile = new System.Windows.Forms.TextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.splitTabCheckBoxRemoveSourceFiles = new System.Windows.Forms.CheckBox();
			this.splitTabCheckBoxCheckMD5 = new System.Windows.Forms.CheckBox();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.textBox1 = new System.Windows.Forms.NumericUpDown();
			this.tabControl = new System.Windows.Forms.TabControl();
			this.tabPage1Split = new System.Windows.Forms.TabPage();
			this.panel2 = new System.Windows.Forms.Panel();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.splitTabSplitButton = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.splitTabSplitDirectoryBrowse = new System.Windows.Forms.Button();
			this.splitTabTextBoxSplitDirectory = new System.Windows.Forms.TextBox();
			this.tabPage2Merge = new System.Windows.Forms.TabPage();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.mergeTabCheckBoxRemoveSourceFiles = new System.Windows.Forms.CheckBox();
			this.mergeTabCheckBoxCheckMD5 = new System.Windows.Forms.CheckBox();
			this.mergeTabMergeButton = new System.Windows.Forms.Button();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.button2 = new System.Windows.Forms.Button();
			this.mergeTabDestFile = new System.Windows.Forms.TextBox();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FileGrouped = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FilePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.mergeTabContextMenuStripMergeFiles = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mergeTabContextMenuStripMergeFilesMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.mergeTabContextMenuStripMergeFilesMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.mergeTabContextMenuStripMergeFilesMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.SplitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.MergeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.groupBox1.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			this.tabControl.SuspendLayout();
			this.tabPage1Split.SuspendLayout();
			this.panel2.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.panel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tabPage2Merge.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.mergeTabContextMenuStripMergeFiles.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.splitTabSplitFileBrowse);
			this.groupBox1.Controls.Add(this.splitTabTextBoxSplitFile);
			this.groupBox1.Location = new System.Drawing.Point(5, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(385, 43);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Файл для разделения";
			// 
			// splitTabSplitFileBrowse
			// 
			this.splitTabSplitFileBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.splitTabSplitFileBrowse.Location = new System.Drawing.Point(332, 15);
			this.splitTabSplitFileBrowse.Name = "splitTabSplitFileBrowse";
			this.splitTabSplitFileBrowse.Size = new System.Drawing.Size(50, 22);
			this.splitTabSplitFileBrowse.TabIndex = 2;
			this.splitTabSplitFileBrowse.Text = "Обзор";
			this.splitTabSplitFileBrowse.UseVisualStyleBackColor = true;
			this.splitTabSplitFileBrowse.Click += new System.EventHandler(this.browseFragFile_Click);
			// 
			// splitTabTextBoxSplitFile
			// 
			this.splitTabTextBoxSplitFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitTabTextBoxSplitFile.Location = new System.Drawing.Point(3, 16);
			this.splitTabTextBoxSplitFile.Name = "splitTabTextBoxSplitFile";
			this.splitTabTextBoxSplitFile.Size = new System.Drawing.Size(330, 20);
			this.splitTabTextBoxSplitFile.TabIndex = 0;
			this.splitTabTextBoxSplitFile.DoubleClick += new System.EventHandler(this.browseFragFile_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.splitTabCheckBoxRemoveSourceFiles);
			this.groupBox3.Controls.Add(this.splitTabCheckBoxCheckMD5);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox3.Location = new System.Drawing.Point(0, 43);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(191, 219);
			this.groupBox3.TabIndex = 6;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Опции";
			// 
			// splitTabCheckBoxRemoveSourceFiles
			// 
			this.splitTabCheckBoxRemoveSourceFiles.AutoSize = true;
			this.splitTabCheckBoxRemoveSourceFiles.Location = new System.Drawing.Point(7, 43);
			this.splitTabCheckBoxRemoveSourceFiles.Name = "splitTabCheckBoxRemoveSourceFiles";
			this.splitTabCheckBoxRemoveSourceFiles.Size = new System.Drawing.Size(150, 17);
			this.splitTabCheckBoxRemoveSourceFiles.TabIndex = 1;
			this.splitTabCheckBoxRemoveSourceFiles.Text = "Удалить исходный файл";
			this.splitTabCheckBoxRemoveSourceFiles.UseVisualStyleBackColor = true;
			// 
			// splitTabCheckBoxCheckMD5
			// 
			this.splitTabCheckBoxCheckMD5.AutoSize = true;
			this.splitTabCheckBoxCheckMD5.Checked = true;
			this.splitTabCheckBoxCheckMD5.CheckState = System.Windows.Forms.CheckState.Checked;
			this.splitTabCheckBoxCheckMD5.Location = new System.Drawing.Point(7, 20);
			this.splitTabCheckBoxCheckMD5.Name = "splitTabCheckBoxCheckMD5";
			this.splitTabCheckBoxCheckMD5.Size = new System.Drawing.Size(125, 17);
			this.splitTabCheckBoxCheckMD5.TabIndex = 0;
			this.splitTabCheckBoxCheckMD5.Text = "Использовать MD5";
			this.splitTabCheckBoxCheckMD5.UseVisualStyleBackColor = true;
			// 
			// comboBox1
			// 
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.IntegralHeight = false;
			this.comboBox1.ItemHeight = 13;
			this.comboBox1.Items.AddRange(new object[] {
            "Кб",
            "Мб",
            "Гб"});
			this.comboBox1.Location = new System.Drawing.Point(143, 15);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(41, 21);
			this.comboBox1.TabIndex = 4;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox1.Location = new System.Drawing.Point(9, 15);
			this.textBox1.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(137, 21);
			this.textBox1.TabIndex = 2;
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBox1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.tabPage1Split);
			this.tabControl.Controls.Add(this.tabPage2Merge);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.Location = new System.Drawing.Point(0, 24);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(598, 294);
			this.tabControl.TabIndex = 2;
			// 
			// tabPage1Split
			// 
			this.tabPage1Split.Controls.Add(this.panel2);
			this.tabPage1Split.Controls.Add(this.panel1);
			this.tabPage1Split.Location = new System.Drawing.Point(4, 22);
			this.tabPage1Split.Name = "tabPage1Split";
			this.tabPage1Split.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1Split.Size = new System.Drawing.Size(590, 268);
			this.tabPage1Split.TabIndex = 0;
			this.tabPage1Split.Text = "Разделение";
			this.tabPage1Split.UseVisualStyleBackColor = true;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.groupBox3);
			this.panel2.Controls.Add(this.groupBox4);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel2.Location = new System.Drawing.Point(396, 3);
			this.panel2.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(191, 262);
			this.panel2.TabIndex = 4;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.comboBox1);
			this.groupBox4.Controls.Add(this.textBox1);
			this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox4.Location = new System.Drawing.Point(0, 0);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(191, 43);
			this.groupBox4.TabIndex = 5;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Размер части";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.splitTabSplitButton);
			this.panel1.Controls.Add(this.groupBox2);
			this.panel1.Controls.Add(this.groupBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(3, 3);
			this.panel1.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(584, 262);
			this.panel1.TabIndex = 2;
			// 
			// splitTabSplitButton
			// 
			this.splitTabSplitButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.splitTabSplitButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.splitTabSplitButton.Location = new System.Drawing.Point(140, 130);
			this.splitTabSplitButton.Name = "splitTabSplitButton";
			this.splitTabSplitButton.Size = new System.Drawing.Size(115, 40);
			this.splitTabSplitButton.TabIndex = 4;
			this.splitTabSplitButton.Text = "Разделить";
			this.splitTabSplitButton.UseVisualStyleBackColor = true;
			this.splitTabSplitButton.Click += new System.EventHandler(this.splitTabSplitButton_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.splitTabSplitDirectoryBrowse);
			this.groupBox2.Controls.Add(this.splitTabTextBoxSplitDirectory);
			this.groupBox2.Location = new System.Drawing.Point(5, 46);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(385, 43);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Папка получатель";
			// 
			// splitTabSplitDirectoryBrowse
			// 
			this.splitTabSplitDirectoryBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.splitTabSplitDirectoryBrowse.Location = new System.Drawing.Point(332, 15);
			this.splitTabSplitDirectoryBrowse.Name = "splitTabSplitDirectoryBrowse";
			this.splitTabSplitDirectoryBrowse.Size = new System.Drawing.Size(50, 22);
			this.splitTabSplitDirectoryBrowse.TabIndex = 2;
			this.splitTabSplitDirectoryBrowse.Text = "Обзор";
			this.splitTabSplitDirectoryBrowse.UseVisualStyleBackColor = true;
			this.splitTabSplitDirectoryBrowse.Click += new System.EventHandler(this.browseFragFolder_Click);
			// 
			// splitTabTextBoxSplitDirectory
			// 
			this.splitTabTextBoxSplitDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitTabTextBoxSplitDirectory.Location = new System.Drawing.Point(3, 16);
			this.splitTabTextBoxSplitDirectory.Name = "splitTabTextBoxSplitDirectory";
			this.splitTabTextBoxSplitDirectory.Size = new System.Drawing.Size(330, 20);
			this.splitTabTextBoxSplitDirectory.TabIndex = 0;
			this.splitTabTextBoxSplitDirectory.DoubleClick += new System.EventHandler(this.browseFragFolder_Click);
			// 
			// tabPage2Merge
			// 
			this.tabPage2Merge.Controls.Add(this.panel3);
			this.tabPage2Merge.Location = new System.Drawing.Point(4, 22);
			this.tabPage2Merge.Name = "tabPage2Merge";
			this.tabPage2Merge.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2Merge.Size = new System.Drawing.Size(590, 268);
			this.tabPage2Merge.TabIndex = 1;
			this.tabPage2Merge.Text = "Объединение";
			this.tabPage2Merge.UseVisualStyleBackColor = true;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.panel4);
			this.panel3.Controls.Add(this.mergeTabMergeButton);
			this.panel3.Controls.Add(this.groupBox5);
			this.panel3.Controls.Add(this.groupBox6);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(3, 3);
			this.panel3.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(584, 262);
			this.panel3.TabIndex = 3;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.groupBox7);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel4.Location = new System.Drawing.Point(393, 0);
			this.panel4.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(191, 262);
			this.panel4.TabIndex = 5;
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.mergeTabCheckBoxRemoveSourceFiles);
			this.groupBox7.Controls.Add(this.mergeTabCheckBoxCheckMD5);
			this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox7.Location = new System.Drawing.Point(0, 0);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(191, 262);
			this.groupBox7.TabIndex = 6;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Опции";
			// 
			// mergeTabCheckBoxRemoveSourceFiles
			// 
			this.mergeTabCheckBoxRemoveSourceFiles.AutoSize = true;
			this.mergeTabCheckBoxRemoveSourceFiles.Location = new System.Drawing.Point(7, 43);
			this.mergeTabCheckBoxRemoveSourceFiles.Name = "mergeTabCheckBoxRemoveSourceFiles";
			this.mergeTabCheckBoxRemoveSourceFiles.Size = new System.Drawing.Size(158, 17);
			this.mergeTabCheckBoxRemoveSourceFiles.TabIndex = 1;
			this.mergeTabCheckBoxRemoveSourceFiles.Text = "Удалить исходные файлы";
			this.mergeTabCheckBoxRemoveSourceFiles.UseVisualStyleBackColor = true;
			// 
			// mergeTabCheckBoxCheckMD5
			// 
			this.mergeTabCheckBoxCheckMD5.AutoSize = true;
			this.mergeTabCheckBoxCheckMD5.Checked = true;
			this.mergeTabCheckBoxCheckMD5.CheckState = System.Windows.Forms.CheckState.Checked;
			this.mergeTabCheckBoxCheckMD5.Location = new System.Drawing.Point(7, 20);
			this.mergeTabCheckBoxCheckMD5.Name = "mergeTabCheckBoxCheckMD5";
			this.mergeTabCheckBoxCheckMD5.Size = new System.Drawing.Size(125, 17);
			this.mergeTabCheckBoxCheckMD5.TabIndex = 0;
			this.mergeTabCheckBoxCheckMD5.Text = "Использовать MD5";
			this.mergeTabCheckBoxCheckMD5.UseVisualStyleBackColor = true;
			// 
			// mergeTabMergeButton
			// 
			this.mergeTabMergeButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.mergeTabMergeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.mergeTabMergeButton.Location = new System.Drawing.Point(141, 217);
			this.mergeTabMergeButton.Name = "mergeTabMergeButton";
			this.mergeTabMergeButton.Size = new System.Drawing.Size(115, 40);
			this.mergeTabMergeButton.TabIndex = 4;
			this.mergeTabMergeButton.Text = "Объединить";
			this.mergeTabMergeButton.UseVisualStyleBackColor = true;
			this.mergeTabMergeButton.Click += new System.EventHandler(this.mergeTabMergeButton_Click);
			// 
			// groupBox5
			// 
			this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox5.Controls.Add(this.button2);
			this.groupBox5.Controls.Add(this.mergeTabDestFile);
			this.groupBox5.Location = new System.Drawing.Point(5, 168);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(385, 43);
			this.groupBox5.TabIndex = 3;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Файл получатель";
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.Location = new System.Drawing.Point(332, 15);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(50, 22);
			this.button2.TabIndex = 2;
			this.button2.Text = "Обзор";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.browseDestFile_Click);
			// 
			// mergeTabDestFile
			// 
			this.mergeTabDestFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.mergeTabDestFile.Location = new System.Drawing.Point(3, 16);
			this.mergeTabDestFile.Name = "mergeTabDestFile";
			this.mergeTabDestFile.Size = new System.Drawing.Size(330, 20);
			this.mergeTabDestFile.TabIndex = 0;
			this.mergeTabDestFile.DoubleClick += new System.EventHandler(this.browseDestFile_Click);
			// 
			// groupBox6
			// 
			this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox6.Controls.Add(this.dataGridView1);
			this.groupBox6.Location = new System.Drawing.Point(5, 0);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(385, 162);
			this.groupBox6.TabIndex = 0;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Файлы для объединения";
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToResizeRows = false;
			this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FileName,
            this.fileSize,
            this.FileGrouped,
            this.FilePath});
			this.dataGridView1.ContextMenuStrip = this.mergeTabContextMenuStripMergeFiles;
			this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dataGridView1.Location = new System.Drawing.Point(3, 16);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(379, 143);
			this.dataGridView1.TabIndex = 0;
			this.dataGridView1.DoubleClick += new System.EventHandler(this.browseMergeFiles_Click);
			// 
			// FileName
			// 
			this.FileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.FileName.DataPropertyName = "fileName";
			this.FileName.HeaderText = "Имя файла";
			this.FileName.Name = "FileName";
			this.FileName.ReadOnly = true;
			this.FileName.Width = 89;
			// 
			// fileSize
			// 
			this.fileSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.fileSize.DataPropertyName = "fileSize";
			this.fileSize.HeaderText = "Размер";
			this.fileSize.Name = "fileSize";
			this.fileSize.ReadOnly = true;
			this.fileSize.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
			this.fileSize.Width = 71;
			// 
			// FileGrouped
			// 
			this.FileGrouped.DataPropertyName = "fileGrouped";
			this.FileGrouped.HeaderText = "Последовательность";
			this.FileGrouped.Name = "FileGrouped";
			this.FileGrouped.ReadOnly = true;
			this.FileGrouped.Visible = false;
			// 
			// FilePath
			// 
			this.FilePath.DataPropertyName = "filePath";
			this.FilePath.HeaderText = "Путь к файлу";
			this.FilePath.Name = "FilePath";
			this.FilePath.ReadOnly = true;
			// 
			// mergeTabContextMenuStripMergeFiles
			// 
			this.mergeTabContextMenuStripMergeFiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mergeTabContextMenuStripMergeFilesMenuItem1,
            this.mergeTabContextMenuStripMergeFilesMenuItem2,
            this.mergeTabContextMenuStripMergeFilesMenuItem3});
			this.mergeTabContextMenuStripMergeFiles.Name = "mergeTabContextMenuStripMergeFiles";
			this.mergeTabContextMenuStripMergeFiles.Size = new System.Drawing.Size(266, 70);
			// 
			// mergeTabContextMenuStripMergeFilesMenuItem1
			// 
			this.mergeTabContextMenuStripMergeFilesMenuItem1.Name = "mergeTabContextMenuStripMergeFilesMenuItem1";
			this.mergeTabContextMenuStripMergeFilesMenuItem1.Size = new System.Drawing.Size(265, 22);
			this.mergeTabContextMenuStripMergeFilesMenuItem1.Text = "&Добавить файлы";
			this.mergeTabContextMenuStripMergeFilesMenuItem1.Click += new System.EventHandler(this.browseMergeFiles_Click);
			// 
			// mergeTabContextMenuStripMergeFilesMenuItem2
			// 
			this.mergeTabContextMenuStripMergeFilesMenuItem2.Name = "mergeTabContextMenuStripMergeFilesMenuItem2";
			this.mergeTabContextMenuStripMergeFilesMenuItem2.Size = new System.Drawing.Size(265, 22);
			this.mergeTabContextMenuStripMergeFilesMenuItem2.Text = "&Удалить выделенные файлы";
			this.mergeTabContextMenuStripMergeFilesMenuItem2.Click += new System.EventHandler(this.removeMergeFiles_Click);
			// 
			// mergeTabContextMenuStripMergeFilesMenuItem3
			// 
			this.mergeTabContextMenuStripMergeFilesMenuItem3.Name = "mergeTabContextMenuStripMergeFilesMenuItem3";
			this.mergeTabContextMenuStripMergeFilesMenuItem3.Size = new System.Drawing.Size(265, 22);
			this.mergeTabContextMenuStripMergeFilesMenuItem3.Text = "&Очистить файлы для объединения";
			this.mergeTabContextMenuStripMergeFilesMenuItem3.Click += new System.EventHandler(this.cleanMergeFiles_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.aboutToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(598, 24);
			this.menuStrip1.TabIndex = 3;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SplitToolStripMenuItem,
            this.MergeToolStripMenuItem,
            this.ExitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
			this.fileToolStripMenuItem.Text = "Файл";
			// 
			// SplitToolStripMenuItem
			// 
			this.SplitToolStripMenuItem.Name = "SplitToolStripMenuItem";
			this.SplitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.SplitToolStripMenuItem.Text = "Разделение";
			this.SplitToolStripMenuItem.Click += new System.EventHandler(this.SplitToolStripMenuItem_Click);
			// 
			// MergeToolStripMenuItem
			// 
			this.MergeToolStripMenuItem.Name = "MergeToolStripMenuItem";
			this.MergeToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.MergeToolStripMenuItem.Text = "Объединение";
			this.MergeToolStripMenuItem.Click += new System.EventHandler(this.MergeToolStripMenuItem_Click);
			// 
			// ExitToolStripMenuItem
			// 
			this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
			this.ExitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.ExitToolStripMenuItem.Text = "Выход";
			this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
			this.helpToolStripMenuItem.Text = "Справка";
			this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
			this.aboutToolStripMenuItem.Text = "О программе";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(598, 318);
			this.Controls.Add(this.tabControl);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Main";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Файловый фрагментатор";
			this.Load += new System.EventHandler(this.Main_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			this.tabControl.ResumeLayout(false);
			this.tabPage1Split.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.tabPage2Merge.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.groupBox7.ResumeLayout(false);
			this.groupBox7.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.groupBox6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.mergeTabContextMenuStripMergeFiles.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox splitTabTextBoxSplitFile;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabPage1Split;
		private System.Windows.Forms.TabPage tabPage2Merge;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem SplitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem MergeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button splitTabSplitFileBrowse;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button splitTabSplitDirectoryBrowse;
		private System.Windows.Forms.TextBox splitTabTextBoxSplitDirectory;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.NumericUpDown textBox1;
		private System.Windows.Forms.CheckBox splitTabCheckBoxRemoveSourceFiles;
		private System.Windows.Forms.CheckBox splitTabCheckBoxCheckMD5;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Button splitTabSplitButton;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Button mergeTabMergeButton;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox mergeTabDestFile;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.CheckBox mergeTabCheckBoxRemoveSourceFiles;
		private System.Windows.Forms.CheckBox mergeTabCheckBoxCheckMD5;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.ContextMenuStrip mergeTabContextMenuStripMergeFiles;
		private System.Windows.Forms.ToolStripMenuItem mergeTabContextMenuStripMergeFilesMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem mergeTabContextMenuStripMergeFilesMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem mergeTabContextMenuStripMergeFilesMenuItem3;
		private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
		private System.Windows.Forms.DataGridViewTextBoxColumn fileSize;
		private System.Windows.Forms.DataGridViewTextBoxColumn FileGrouped;
		private System.Windows.Forms.DataGridViewTextBoxColumn FilePath;
	}
}

