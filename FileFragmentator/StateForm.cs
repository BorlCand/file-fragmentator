﻿using FileFragmentator.FileOperations;
using System;
using System.Windows.Forms;

namespace FileFragmentator {
	public partial class StateForm : Form {

		public delegate void StateFormCancelEventHandler();
		public static event StateFormCancelEventHandler cancelFileOperations;

		public StateForm() {
			InitializeComponent();
			FileOperation.newSettings += FileOperation_newSettings;
		}

		private void FileOperation_newSettings(string title, string message, int progress, bool? state){
			if (this.InvokeRequired){
				FileOperation.FileOperationNewSettingsEventHandler settings = FileOperation_newSettings;
				this.Invoke(settings, new object[]{title, message, progress, state
				});
			}

			this.Text = title;
			this.messageText.Text = message;
			switch (progress){
				case -1:
					this.progressBar1.Style = ProgressBarStyle.Marquee;
				break;

				case -2:
					this.cancelButton.Enabled = true;
					this.closeButton.Enabled = false;
				break;

				case -3:
					this.cancelButton.Enabled = false;
					this.closeButton.Enabled = true;
					//this.progressBar1.Size = new Size(0, 0);
				break;

				case -4:
					this.cancelButton.Enabled = false;
					this.closeButton.Enabled = true;
				break;

				default:
					this.progressBar1.Style = ProgressBarStyle.Blocks;
					this.progressBar1.Value = progress;
				break;
			}
		}

		private void button1_Click(object sender, EventArgs e){
			if (cancelFileOperations != null){
				this.cancelButton.Enabled = false;
				cancelFileOperations();
			}
		}

		private void closeButton_Click(object sender, EventArgs e) {
			this.Close();
			this.Dispose();
		}

		private void messageText_TextChanged(object sender, EventArgs e){
			this.Height = messageText.PreferredSize.Height + 130;
		}

		private void StateForm_FormClosed(object sender, FormClosedEventArgs e) {
			this.Dispose();
		}
	}
}
