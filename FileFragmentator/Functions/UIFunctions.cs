﻿using System;
using System.Text.RegularExpressions;

namespace FileFragmentator.Functions {
	public static class Ui {

		public static long getPartSize(long size, int measure) {
			switch (measure) {
				case 0:
					return size * 1024;
				case 1:
					return size * 1024 * 1024;
				case 2:
					return size * 1024 * 1024 * 1024;
				default:
					return size*1024;
			}

		}

		[Obsolete]
		public static bool isNumeric(string text) {
			Regex regex = new Regex("[^0-9]+");
			return !regex.IsMatch(text);
		}
	}
}
