﻿using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace FileFragmentator.Functions {
	public static class Io {

		/**
		 * <summary>
		 *	Диалог выбора файлов для всех событий
		 *	by BorlCand
		 *	@ 2:35 21/02/2014
		 * </summary>
		 * <param name="multipleFiles">Определяет возможность выбора множеств файлов</param>
		 * <returns>
		 *	Массив string содержащий все полные пути к выбранным файлам
		 * </returns>
		 **/
		public static string[] selectFiles(bool multipleFiles) {
			// Создание экземпляра OpenFileDialog
			OpenFileDialog dlg = new OpenFileDialog{
				Title = "Выберите файл(ы)", DefaultExt = "*.*", Filter = "Все файлы (*.*)|*.*",CheckFileExists = true, CheckPathExists = true, 
			};

			// Настройка различных параметров диалога
			if (multipleFiles == true)
				dlg.Multiselect = true;
			// Отображение OpenFileDialog вызовом метода ShowDialog
			//Old <Nullable>bull
			DialogResult result = dlg.ShowDialog();
			
			// Проверка на результат. Если положительный - возвращаем полный путь к файлу, если нет возвращаем null
			if (result == DialogResult.OK) {
				if (multipleFiles == true) {
					return dlg.FileNames;
				} else {
					string[] filenames = new string[1];
					filenames[0] = dlg.FileName;

					return filenames;
				}
			} else
				return null;
		}

		/**
		 * <summary>
		 * Диалог выбора каталога для всех событий
		 * by BorlCand
		 * @ 2:49 21/02/2014
		 * </summary>
		 * <returns>
		 *	Полный путь к выбранному каталогу
		 * </returns>
		 **/
		public static string selectFolder() {
			using (FolderBrowserDialog dlg = new FolderBrowserDialog()) {
				dlg.Description = "Выберите папку:";
				DialogResult result = dlg.ShowDialog();
				if (result == DialogResult.OK){
					return dlg.SelectedPath;
				} else{
					return null;
				}
			}
		}

		[DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool GetDiskFreeSpaceEx(string lpDirectoryName, out ulong lpFreeBytesAvailable, out ulong lpTotalNumberOfBytes, out ulong lpTotalNumberOfFreeBytes);

		public static bool enoughFreeSpace(long size, string path){
			ulong freeBytesAvailable;
			ulong totalNumberOfBytes;
			ulong totalNumberOfFreeBytes;

			bool success = GetDiskFreeSpaceEx(path, out freeBytesAvailable, out totalNumberOfBytes, out totalNumberOfFreeBytes);

			if (!success){
				return false;
			}
			return freeBytesAvailable >= (ulong)size;
		}

		/**
		 * <summary>
		 *	Диалог выбора файла для сохранения
		 *	by BorlCand
		 *	@ 2:35 21/02/2014
		 * </summary>
		 * <returns>
		 *	string содержащий полный путь к выбранному файлу для сохранения
		 * </returns>
		 **/
		public static string saveFile(){
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.OverwritePrompt = true;
			dlg.CheckPathExists = true;

			DialogResult result = dlg.ShowDialog();

			// Проверка на результат. Если положительный - возвращаем полный путь к файлу, если нет возвращаем null
			if (result == DialogResult.OK){
				if (!string.IsNullOrEmpty(dlg.FileName)){
					return dlg.FileName;
				} else
					return null;
			} else
				return null;
		}

	}
}
