﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace FileFragmentator {
	public partial class AboutForm : Form {
		public AboutForm() {
			InitializeComponent();
			label2.Text = "Версия: " + ProductVersion;
		}

		private void AboutForm_FormClosed(object sender, FormClosedEventArgs e) {
			Dispose();
		}

		private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
			System.Diagnostics.Process.Start("mailto:"+ linkLabel1.Text);
		}

		private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
			System.Diagnostics.Process.Start(linkLabel2.Text);
		}

		private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
			System.Diagnostics.Process.Start(linkLabel3.Text);
		}

		private void button1_Click(object sender, EventArgs e) {
			Close();
			Dispose();
		}
	}
}
