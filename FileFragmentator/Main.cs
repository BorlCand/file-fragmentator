﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using FileFragmentator.FileOperations;
using FileFragmentator.Functions;
using FileFragmentator.Structures;

namespace FileFragmentator {
	public partial class Main : Form {

		#region Defines
		//Defining usability flags
		private bool flagUserDefinedMergePath;
		private bool flagUserDefinedSplitPath;

		//Defining structures
		private List<MergeFile> mergeList = new List<MergeFile>();
		private BindingSource bindingSource = new BindingSource();
		
		//Defining form objects
		private HelpForm helpForm;
		private StateForm stateForm;

		#endregion Defines

		public Main() {
			InitializeComponent();
			bindingSource.ListChanged += bindingSource_DataSourceChanged;
		}

		void bindingSource_DataSourceChanged(object sender, EventArgs e) {
			if (flagUserDefinedMergePath)
				return;

			if (bindingSource.Count == 0) {
				mergeTabDestFile.Text = "";
			} else{
				if (Path.GetExtension(((MergeFile)bindingSource[0]).fileName) == ".bff"){
					mergeTabDestFile.Text = Path.Combine(Path.GetDirectoryName(((MergeFile)bindingSource[0]).filePath), Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(((MergeFile)bindingSource[0]).filePath)));
				} else{
					mergeTabDestFile.Text = "";
				}
			}
		}

		private void SplitToolStripMenuItem_Click(object sender, EventArgs e) {
			tabControl.SelectTab(0);
		}

		private void MergeToolStripMenuItem_Click(object sender, EventArgs e) {
			tabControl.SelectTab(1);
		}

		private void removeMergeFiles_Click(object sender, EventArgs e) {
			if (mergeList.Count == 0)
				return;
			var selectedItems = dataGridView1.SelectedRows;

			for (var i = selectedItems.Count - 1; i >= 0; i--) {
				object file = bindingSource[selectedItems[i].Index];
				bindingSource.RemoveAt(bindingSource.IndexOf(file));
			}
		}

		private void browseMergeFiles_Click(object sender, EventArgs e) {
			string[] filename = Io.selectFiles(true);
			if (filename != null) {
				foreach (string file in filename) {
					if (Path.GetExtension(Path.GetFileNameWithoutExtension(file)) == ".md5")
						continue;
					bindingSource.Add(new MergeFile(Path.GetFileName(file), String.Format(new FileSizeFormatProvider(), "{0:fs}", new FileInfo(file).Length), true, file));
				}
			}
		}

		private void cleanMergeFiles_Click(object sender, EventArgs e) {
			bindingSource.Clear();
		}

		private void browseFragFile_Click(object sender, EventArgs e) {
			string[] filename = Io.selectFiles(false);
			if (filename != null) {
				splitTabTextBoxSplitFile.Text = filename[0];
				if (!flagUserDefinedSplitPath){
					splitTabTextBoxSplitDirectory.Text = Path.GetDirectoryName(filename[0]);
				}
			}
		}

		private void browseFragFolder_Click(object sender, EventArgs e) {
			string path = Io.selectFolder();
			if (!string.IsNullOrEmpty(path)) {
				splitTabTextBoxSplitDirectory.Text = path;
				flagUserDefinedSplitPath = true;
			}
		}

		private void browseDestFile_Click(object sender, EventArgs e) {
			string filename = Io.saveFile();
			if (!string.IsNullOrEmpty(filename)) {
				mergeTabDestFile.Text = filename;
				flagUserDefinedMergePath = true;
			}

		}

		private void ExitToolStripMenuItem_Click(object sender, EventArgs e) {
			Application.Exit();
		}

		private void Main_Load(object sender, EventArgs e){
			comboBox1.SelectedIndex = 1;
			dataGridView1.DataSource = bindingSource;
			bindingSource.DataSource = mergeList;
		}

		private void splitTabSplitButton_Click(object sender, EventArgs e) {
			new FileSplitter(splitTabTextBoxSplitFile.Text, splitTabTextBoxSplitDirectory.Text, Ui.getPartSize(long.Parse(this.textBox1.Text), this.comboBox1.SelectedIndex), splitTabCheckBoxCheckMD5.Checked, splitTabCheckBoxRemoveSourceFiles.Checked);
			new StateForm().ShowDialog();		
		}

		private void mergeTabMergeButton_Click(object sender, EventArgs e){
			new FileMerger(mergeList, mergeTabDestFile.Text, mergeTabCheckBoxCheckMD5.Checked, mergeTabCheckBoxRemoveSourceFiles.Checked);
			stateForm = new StateForm();
			stateForm.ShowDialog();
		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
			new AboutForm().ShowDialog();
		}

		private void helpToolStripMenuItem_Click(object sender, EventArgs e) {
			if (helpForm!= null && !helpForm.IsDisposed){
				helpForm.Focus();
			} else{
				helpForm = new HelpForm();
				helpForm.Show();
			}
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {
			switch (comboBox1.SelectedIndex) {
				
				case 0:
					textBox1.Maximum = 1000000000;
					break;

				case 1:
					textBox1.Maximum = 1000000;
					break;

				case 2:
					textBox1.Maximum = 1000;
					break;
			}
		}
	}
}
