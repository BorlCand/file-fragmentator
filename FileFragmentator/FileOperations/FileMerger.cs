﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using FileFragmentator.Functions;
using FileFragmentator.Structures;

namespace FileFragmentator.FileOperations {
	public sealed class FileMerger : FileOperation {

		private HashAlgorithm hasher;

		private List<MergeFile> source;
		private string dest;
		private bool check;
		private bool? hashChecked;
		private long totalFileSize;
		private long currentFileSize;
		private bool removeSourceFile;
		private bool isCanceled;
		private bool isNotEnoughSpace;

		public FileMerger(List<MergeFile> source, string dest, bool check, bool removeSourceFile) {
			this.source = source;
			this.dest = dest;
			this.check = check;
			this.removeSourceFile = removeSourceFile;
			hashChecked = null;
			thread.Name = "FileMerger Thread";
			StateForm.cancelFileOperations += StateForm_cancelFileOperations;
		}

		protected override void runtimeQueue() {
			sortMergeFiles(source);

			newSettingsEvent("Объединение", "Проверка данных...", -1, true);

			if (check) {
				hasher = MD5.Create();
				hasher.Initialize();
			}

			const long BUFFER_SIZE = 10 * 1024 * 1024; //10MB Buffer для чтения, иначе скорость маленькая
			byte[] buffer = new byte[BUFFER_SIZE];
			int index = 0;
			try {
				if (string.IsNullOrEmpty(dest)) {
					throw new IOException("Путь сохранения не верен.");
				}
				if (Directory.Exists(dest)){
					throw new IOException("Путь сохранения является папкой");
				} else{
					if (!Directory.Exists(Directory.GetParent(dest).FullName)){
						Directory.CreateDirectory(Directory.GetParent(dest).FullName);
					}
				}
				if (source.Count == 0) {
					throw new IOException("Нет файлов для объединения");
				}

				foreach (MergeFile file in source) {
					FileInfo info = new FileInfo(file.filePath);
					totalFileSize += info.Length;
				}

				using (FileStream output = new FileStream(dest, FileMode.Create, FileAccess.Write, FileShare.None)) {
					foreach (MergeFile sourceFile in source) {
						using (FileStream input = new FileStream(sourceFile.filePath, FileMode.Open, FileAccess.Read, FileShare.None)) {
							while (input.Position < input.Length) {

								index++;

								long remaining = input.Length;
								int bytesRead;

								newSettingsEvent("Объединение", "Объединяем файлы: файл " + index + " из " + source.Count + "...\n" + sourceFile.fileName, -2, true);

								if (isCanceled)
									break;

								while (remaining > 0 && (bytesRead = input.Read(buffer, 0, (int)Math.Min(remaining, BUFFER_SIZE))) > 0) {
									if (!Io.enoughFreeSpace(Math.Min(remaining, BUFFER_SIZE), Path.GetDirectoryName(dest))) {
										isCanceled = true;
										isNotEnoughSpace = true;
									}

									if (isCanceled)
										break;

									output.Write(buffer, 0, bytesRead);
									remaining -= bytesRead;
									currentFileSize += bytesRead;

									if (check)
										hasher.TransformBlock(buffer, 0, bytesRead, null, 0);

									int progress = (int)(((double)currentFileSize / (double)totalFileSize) * 100);
									newSettingsEvent("Объединение", "Объединяем файлы: файл " + index + " из " + source.Count + "...\n" + sourceFile.fileName, progress, true);
								}
							}
						}
						if (removeSourceFile && !isCanceled) {
							File.Delete(sourceFile.filePath);
						}
					}
				}


				if (check && !isCanceled) {
					if (File.Exists(Path.Combine(Path.GetDirectoryName(source[0].filePath), (Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(source[0].filePath)) + ".md5.bff")))) {
						using (FileStream input = new FileStream(Path.Combine(Path.GetDirectoryName(source[0].filePath), (Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(source[0].filePath)) + ".md5.bff")), FileMode.Open, FileAccess.Read, FileShare.None)) {
							hasher.TransformFinalBlock(new byte[0], 0, 0);
							byte[] hash = hasher.Hash;
							StringBuilder sb = new StringBuilder();
							foreach (byte b in hash)
								sb.Append(b.ToString("X2"));

							byte[] hexBytes = Encoding.UTF8.GetBytes(sb.ToString());
							byte[] inputHexBytes = new byte[input.Length];
							input.Read(inputHexBytes, 0, (int)input.Length);

							if (inputHexBytes.Length != hexBytes.Length){
								hashChecked = null;
							} else{
								for (int i = 0; i < hexBytes.Length; i++){
									if (hexBytes[i] != inputHexBytes[i]){
										hashChecked = false;
										break;
									}
									hashChecked = true;
								}
							}
						}
					}
				}

				if (isNotEnoughSpace) {
					newSettingsEvent("Ошибка", "Недостаточно места на диске", -3, false);
				} else if (isCanceled) {
					newSettingsEvent("Объединение отменено", "Вы отменили процесс объединения!", -4, null);
				} else {
					string message = "Файлы объединены в:\n" + dest;
					if (hashChecked == null) {
						message += "\nФайл хеша не найден";
					} else if (hashChecked == true) {
						message += "\nФайл собран верно";
					} else if (hashChecked == false) {
						message += "\nФайл собран НЕ верно";
					}
					newSettingsEvent("Объединение завершено", message, -4, true);
				}
			} catch (Exception e) {
				newSettingsEvent("Ошибка", e.Message, -3, false);
			}
		}

		private void sortMergeFiles(List<MergeFile> o) {
			for (int i = o.Count - 1; i >= 0; i--) {
				for (int j = 1; j <= i; j++) {
					var o1 = o[j - 1];
					var o2 = o[j];
					if (((IComparable)o1.fileName).CompareTo(o2.fileName) > 0) {
						o.Remove(o1);
						o.Insert(j, o1);
					}
				}
			}
		}

		private void StateForm_cancelFileOperations() {
			this.isCanceled = true;
		}
	}
}
