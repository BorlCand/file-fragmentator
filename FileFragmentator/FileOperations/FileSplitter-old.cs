﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace FileFragmentator.FileOperations {
	class FileSplitter_old {
		private Main main;
		private HashAlgorithm hasher;

		private string sourceFilePath;
		private string destFilePath;
		private Int64 partSize;
		private int chunkSize;
		private ComboBox measureSize;
		private bool useCheck;
		private bool removeSourceFile;

		public FileSplitter_old(Main main, string sourceFilePath, string destFilePath, int chunkSize, ComboBox measureSize, bool useCheck, bool removeSourceFile) {
			this.main = main;
			this.destFilePath = destFilePath;
			this.sourceFilePath = sourceFilePath;
			this.chunkSize = chunkSize;
			this.useCheck = useCheck;
			this.removeSourceFile = removeSourceFile;
			this.measureSize = measureSize;

			setPartSize();
			splitFile();
		}
		
		private async void splitFile(){
			ProgressDialogController controller = await main.ShowProgressAsync("Разделение", "Проверка данных...", false, MessageDialogSettings.negativeDialogStyle);

			await TaskEx.Delay(1000);

			controller.SetCancelable(true);

			if (useCheck){
				hasher = MD5.Create();
				hasher.Initialize();
			}


			const long bufferSize = 10 * 1024 * 1024; //10MB Buffer для чтения, иначе скорость маленькая
			byte[] buffer = new byte[bufferSize];
			int index = 0;
			bool notEnoughFreeSpace = false;

			using (Stream input = File.OpenRead(sourceFilePath)){
				while (!notEnoughFreeSpace && input.Position < input.Length) {
					using (Stream output = File.Create(Path.Combine(destFilePath, Path.GetFileName(sourceFilePath) + "." + index.ToString("000") + ".bff"))){
						index++;

						long remaining = partSize;
						int bytesRead;

						controller.SetMessage("Разделяем файл: часть #" + index + "...");

						if (controller.IsCanceled)
							break;

						while (true){
							if (controller.IsCanceled || notEnoughFreeSpace)
								break;

							while (!IOFunctions.enoughFreeSpace(Math.Min(remaining, bufferSize), destFilePath)){
								await controller.CloseAsync();
								MessageDialogResult controllerMessageDialog = await main.ShowMessageAsync("Ошибка", "Недостаточно места на диске", MessageDialogStyle.AffirmativeAndNegative, MessageDialogSettings.notEnoughFreeSpaceDialogStyle);
								if (controllerMessageDialog == MessageDialogResult.Negative){
									notEnoughFreeSpace = true;
									break;
								} else if (controllerMessageDialog == MessageDialogResult.Affirmative) {
									controller = await main.ShowProgressAsync("Разделение", "Проверка данных...", true, MessageDialogSettings.negativeDialogStyle);
									await TaskEx.Delay(1000);
								}
							}

							if (!notEnoughFreeSpace && remaining > 0 && (bytesRead = input.Read(buffer, 0, (int)Math.Min(remaining, bufferSize))) > 0) {

								output.Write(buffer, 0, bytesRead);
								remaining -= bytesRead;

								if (useCheck)
									hasher.TransformBlock(buffer, 0, bytesRead, null, 0);

								double progressD = (double) input.Position / (double) input.Length;
								controller.SetProgress(progressD);
								await TaskEx.Delay(1);
							} else{
								break;
							}
						}
					}
				}

				if (useCheck && !controller.IsCanceled && !notEnoughFreeSpace) {
					using (Stream output = File.Create(Path.Combine(destFilePath, Path.GetFileName(sourceFilePath) + ".md5.bff"))) {
						hasher.TransformFinalBlock(new byte[0], 0, 0);
						byte[] hash = hasher.Hash;
						StringBuilder sb = new StringBuilder();
						foreach (byte b in hash)
							sb.Append(b.ToString("X2"));

						byte[] hexBytes = Encoding.UTF8.GetBytes(sb.ToString());
						
						output.Write(hexBytes, 0, hexBytes.Length);
					}
				}
			}

			if (removeSourceFile && !controller.IsCanceled && !notEnoughFreeSpace) {
				File.Delete(sourceFilePath);
			}

			if (controller.IsOpen){
				await controller.CloseAsync();
			}

			if (controller.IsCanceled) {
				await main.ShowMessageAsync("Разделение отменено", "Вы отменили процесс разделения!", MessageDialogStyle.Affirmative, MessageDialogSettings.affirmativeDialogStyle);
			} else if (!notEnoughFreeSpace){
				await main.ShowMessageAsync("Разделение завершено", "Файл разделен в папку:\n" + destFilePath + "\nКоличество частей: " + index);
			}

		}

		private void setPartSize() {
			switch (measureSize.SelectedIndex){
				case 0:
					partSize = (long)chunkSize * 1024;
					break;
				case 1:
					partSize = (long)chunkSize * 1024 * 1024;
					break;
				case 2:
					partSize = (long)chunkSize * 1024 * 1024 * 1024;
					break;
			}
		
		}

	}
}
