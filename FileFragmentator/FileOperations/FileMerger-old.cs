﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using FileFragmentator.Structures;

namespace FileFragmentator.FileOperations{
	public class FileMerger_old{

		private Main main;
		private HashAlgorithm hasher;

		private List<MergeFile>sourceFilesPath;
		private string destFilePath;
		private bool useHashCheck;
		private bool removeSourceFile;
		private bool? hashChecked;
		private long totalFileSize;
		private long currentFileSize;

		public FileMerger_old(Main mainwindow, List<MergeFile> source, string dest, bool check, bool remove){
			main = mainwindow;
			sourceFilesPath = source;
			destFilePath = dest;
			useHashCheck = check;
			removeSourceFile = remove;
			hashChecked = null;

			sortMergeFiles(sourceFilesPath);
			getTotalFileSize();
			mergeFile();
		}

		private async void mergeFile(){
			ProgressDialogController controller = await main.ShowProgressAsync("Объединение", "Проверка данных...");

			await TaskEx.Delay(1000);

			controller.SetCancelable(true);

			if (useHashCheck){
				hasher = MD5.Create();
				hasher.Initialize();
			}


			const long bufferSize = 10*1024*1024; //10MB Buffer для чтения, иначе скорость маленькая
			byte[] buffer = new byte[bufferSize];
			int index = 0;
			bool notEnoughFreeSpace = false;

			using (Stream output = File.Create(destFilePath)){
				foreach (MergeFile sourceFile in sourceFilesPath){
					using (Stream input = File.OpenRead(sourceFile.filePath)){
						while (!notEnoughFreeSpace && input.Position < input.Length){

							index++;

							long remaining = input.Length;
							int bytesRead;

							controller.SetMessage("Объединяем файлы: файл " + index + " из " + sourceFilesPath.Count + "...\n" + sourceFile.fileName);

							if (controller.IsCanceled )
								break;

							while (true){
								if (controller.IsCanceled || notEnoughFreeSpace)
									break;

								while (!IOFunctions.enoughFreeSpace(Math.Min(remaining, bufferSize), Path.GetDirectoryName(destFilePath))) {
									await controller.CloseAsync();
									MessageDialogResult controllerMessageDialog = await main.ShowMessageAsync("Ошибка", "Недостаточно места на диске", MessageDialogStyle.AffirmativeAndNegative, MessageDialogSettings.notEnoughFreeSpaceDialogStyle);
									if (controllerMessageDialog == MessageDialogResult.Negative) {
										notEnoughFreeSpace = true;
										break;
									} else if (controllerMessageDialog == MessageDialogResult.Affirmative) {
										controller = await main.ShowProgressAsync("Разделение", "Проверка данных...", true, MessageDialogSettings.negativeDialogStyle);
										await TaskEx.Delay(1000);
									}
								}

								if (!notEnoughFreeSpace && remaining > 0 && (bytesRead = input.Read(buffer, 0, (int)Math.Min(remaining, bufferSize))) > 0) {
									output.Write(buffer, 0, bytesRead);
									remaining -= bytesRead;
									currentFileSize += bytesRead;

									if (useHashCheck)
										hasher.TransformBlock(buffer, 0, bytesRead, null, 0);

									double progressD = (double) currentFileSize/(double) totalFileSize;
									controller.SetProgress(progressD);
									await TaskEx.Delay(1);
								} else {
									break;
								}
							}
						}
					}
					if (removeSourceFile && !controller.IsCanceled && !notEnoughFreeSpace) {
						File.Delete(sourceFile.filePath);
					}
				}
			}


			if (useHashCheck && !controller.IsCanceled && !notEnoughFreeSpace) {
				if (File.Exists(Path.Combine(Path.GetDirectoryName(sourceFilesPath[0].filePath),(Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(sourceFilesPath[0].filePath))+".md5.bff")))) {
					using (Stream input = File.OpenRead(Path.Combine(Path.GetDirectoryName(sourceFilesPath[0].filePath), (Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(sourceFilesPath[0].filePath)) + ".md5.bff")))) {
						hasher.TransformFinalBlock(new byte[0], 0, 0);
						byte[] hash = hasher.Hash;
						StringBuilder sb = new StringBuilder();
						foreach (byte b in hash)
							sb.Append(b.ToString("X2"));

						byte[] hexBytes = Encoding.UTF8.GetBytes(sb.ToString());
						byte[] inputHexBytes = new byte[input.Length];
						input.Read(inputHexBytes, 0, (int)input.Length);

						for (int i = 0; i < hexBytes.Length; i++){
							if (hexBytes[i] != inputHexBytes[i]){
								hashChecked = false;
								break;
							}
							hashChecked = true;
						}

						/* Disabled due to wrong check(arrays dynamic)
							if (hexBytes == inputHexBytes){
								hashChecked = true;
							} else{
								hashChecked = false;
							}
						*/
					}
				}
			}

			if (controller.IsOpen) {
				await controller.CloseAsync();
			}

			if (controller.IsCanceled){
				await main.ShowMessageAsync("Объединение отменено", "Вы отменили процесс объединения!");
			} else if (!notEnoughFreeSpace){
				string message = "Файлы объединены в:\n" + destFilePath;
				if (hashChecked == null){
					message += "\nФайл хеша не найден";
				} else if (hashChecked == true){
					message += "\nФайл собран верно";
				} else if (hashChecked == false){
					message += "\nФайл собран НЕ верно";
				}
				await main.ShowMessageAsync("Объединение завершено", message);
			}

			}


		private void getTotalFileSize() {
			foreach (MergeFile file in sourceFilesPath) {
				FileInfo info = new FileInfo(file.filePath);
				totalFileSize += info.Length;
			}
		}

		private void sortMergeFiles(List<MergeFile> o) {
			for (int i = o.Count - 1; i >= 0; i--) {
				for (int j = 1; j <= i; j++) {
					var o1 = o[j - 1];
					var o2 = o[j];
					if (((IComparable)o1.fileName).CompareTo(o2.fileName) > 0) {
						o.Remove(o1);
						o.Insert(j, o1);
					}
				}
        }
		}

		}
	}