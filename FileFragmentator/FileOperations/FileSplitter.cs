﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using FileFragmentator.Functions;

namespace FileFragmentator.FileOperations {
	public sealed class FileSplitter : FileOperation {

		private HashAlgorithm hasher;
		private string source;
		private string dest;
		private long partSize;
		private bool check;
		private bool removeSourceFile;
		private bool isCanceled;
		private bool isNotEnoughSpace;

		public FileSplitter(string source, string dest, long partSize, bool check, bool removeSourceFile) {
			this.source = source;
			this.dest = dest;
			this.partSize = partSize;
			this.check = check;
			this.removeSourceFile = removeSourceFile;
			thread.Name = "FileSplitter Thread";
			StateForm.cancelFileOperations += StateForm_cancelFileOperations;
		}

		void StateForm_cancelFileOperations() {
			isCanceled = true;
		}

		protected override void runtimeQueue() {
			if (check) {
				hasher = MD5.Create();
				hasher.Initialize();
			}
			newSettingsEvent("Разделение", "Проверка данных...", -1, true);

			const long BUFFER_SIZE = 10 * 1024 * 1024; //10MB Buffer для чтения, иначе скорость маленькая
			byte[] buffer = new byte[BUFFER_SIZE];
			int index = 0;
			
			try{
				if (string.IsNullOrEmpty(dest)) {
					throw new IOException("Путь сохранения не верен.");
				}
				if (!Directory.Exists(dest)) {
					Directory.CreateDirectory(dest);
				}
				if (partSize <= 0) {
					throw new IOException("Размер части не может быть меньше или равным 0.");
				}
				
				using (FileStream input = new FileStream(source, FileMode.Open, FileAccess.Read, FileShare.None)){
					while (input.Position < input.Length){
						using (FileStream output = new FileStream(Path.Combine(dest, Path.GetFileName(source) + "." + index.ToString("000") + ".bff"), FileMode.Create, FileAccess.Write, FileShare.None)){
							index++;

							long remaining = partSize;
							int bytesRead;

							newSettingsEvent("Разделение", "Разделяем файл: часть #" + index + "...", -2, true);

							if (isCanceled)
								break;

							while (remaining > 0 && (bytesRead = input.Read(buffer, 0, (int) Math.Min(remaining, BUFFER_SIZE))) > 0){
								if (!Io.enoughFreeSpace(Math.Min(remaining, BUFFER_SIZE), dest)){
									isCanceled = true;
									isNotEnoughSpace = true;
								}

								if (isCanceled)
									break;

								output.Write(buffer, 0, bytesRead);
								remaining -= bytesRead;

								if (check)
									hasher.TransformBlock(buffer, 0, bytesRead, null, 0);

								int progress = (int) (((double) input.Position/(double) input.Length)*100);
								newSettingsEvent("Разделение", "Разделяем файл: часть #" + index + "...", progress, true);
							}
						}
					}

					if (check && !isCanceled){
						using (FileStream output = new FileStream(Path.Combine(dest, Path.GetFileName(source) + ".md5.bff"), FileMode.Create, FileAccess.Write, FileShare.None)){
							hasher.TransformFinalBlock(new byte[0], 0, 0);
							byte[] hash = hasher.Hash;
							StringBuilder sb = new StringBuilder();
							foreach (byte b in hash)
								sb.Append(b.ToString("X2"));

							byte[] hexBytes = Encoding.UTF8.GetBytes(sb.ToString());

							output.Write(hexBytes, 0, hexBytes.Length);
						}
					}
				}

				if (removeSourceFile && !isCanceled){
					File.Delete(source);
				}
				if (isNotEnoughSpace){
					newSettingsEvent("Ошибка", "Недостаточно места на диске", -3, false);
				} else if (isCanceled){
					newSettingsEvent("Разделение отменено", "Вы отменили процесс разделения!", -3, null);
				} else{
					newSettingsEvent("Разделение завершено", "Файл разделен в папку:\n" + dest + "\nКоличество частей: " + index, -4, true);
				}
			} catch (Exception e) {
				newSettingsEvent("Ошибка", e.Message, -3, false);
			}
		}
	}
}
