﻿using System;
using System.Threading;

namespace FileFragmentator.FileOperations {
	public abstract class FileOperation : IDisposable {
		
		public delegate void FileOperationNewSettingsEventHandler(string title, string message, int progress, bool? state);
		public static event FileOperationNewSettingsEventHandler newSettings;

		protected Thread thread;

		protected FileOperation() {
			thread = new Thread(runtimeQueue);
			thread.Start();
		}

		protected abstract void runtimeQueue();
		
		protected void stopThread() {
			thread.Abort();
		}

		public void Dispose() {
			stopThread();
		}

		protected void newSettingsEvent(string title, string message, int progress, bool? state) {
			if (newSettings!=null)
				newSettings(title, message, progress, state);
		}
	}
}
