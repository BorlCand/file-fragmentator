﻿namespace FileFragmentator.Structures {
	public class MergeFile {

		public MergeFile(string name, string size, bool group, string path){
			this.fileName = name;
			this.fileSize = size;
			this.fileGrouped = group;
			this.filePath = path;
		}
		
		public string fileName {
			get;
			set;
		}
		public string fileSize {
			get;
			set;
		}

		public bool fileGrouped{
			get;
			set;
		}

		public string filePath{
			get;
			set;
		}
	}
}
