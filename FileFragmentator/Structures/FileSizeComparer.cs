﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileManager.Structures {

	[Obsolete]
	public class FileSizeComparer : IComparer<string> {
		public int Compare(string x, string y) {
			var xFields = x.Split('*');
			var yFields = y.Split('*');
			if (xFields[1] == "Гб" && yFields[0] == "Мб")
				return 1;
			if (xFields[1] == "Мб" && yFields[0] == "Гб")
				return -1;
			return int.Parse(yFields[0]) - int.Parse(xFields[0]);
		}
	}
}
